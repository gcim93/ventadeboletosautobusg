#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

                //Escuela Superior de Ingenieria Mecanica y Electrica Unidad Zacatenco
                //Proyecto: Programa para venta de boletos
                //Alumno: Ciriaco Mendez Gil
                //Grupo: 1EV3


//Funcion que nos ayudara a mover el texto segun las coordenadas que coloquemos
    void gotoxy(int x, int y){
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord); }
    void dibujarCuadro(int x1,int y1,int x2,int y2);

int menu();
int boleto();

//Declaracion de nuestra funcion principal
   int main(){

      //Usando la funcion gotoxy, acomodamos nuestro texto
      gotoxy (40,1); printf("************Bienvenido a Bus MX************\n");
      gotoxy (53,3); printf("Servicios a bordo");
      gotoxy (9,5); printf("|Aire Acondicionado|"); printf("       "); printf("|WIFI|"); printf("       "); printf("|Cinturones de Seguridad|"); printf("       "); printf("|Entretenimiento|"); printf("       "); printf("|Comodidad|\n");

      //Declaramos la variable que controlara el menu
      int seleccion;

       //Llamamos a las funciones
       menu(seleccion); //Le pasamos la variable seleccion
       boleto();

       return 0;
}

//Declaramos nuestra funcion Menu para no saturar la funcion principal y le pasamos la variable seleccion
   int menu(int seleccion){


       char nombre[15],apeP[15],apeM[15], asientos[30],horario[10], tipoBole[7];
       int edad,origen,destino, cantBol, tPagar, opcBoleto, descuento, precioFin, salida;
       int acapulco=699, guadalajara=700, mexico=450, pozaR=450;


//Hacemos nuestro ciclo en dado caso que el usuario ingrese una opcion que no corresponda
   do{
      printf("Por favor selecciona una opcion(1,2,3,4)\n");
      printf("1.- Destinos\n");
      printf("2.- Proximas salidas\n");
      printf("3.- Compre sus boletos\n");
      printf("4.- Salir\n");
      printf("\nIngrese su seleccion por favor: ");
      scanf("%i",&seleccion);

//Declaramos un switch con la variable seleccion para controlar el menu
   switch(seleccion){

//Primer caso para los destinos
case 1:
    system("cls");
    printf("*****************************************************\n");
    printf("Bienvenido, te presentamos nuestros destinos\n");
    printf("-Acapulco, GRO\n");
    printf("-Guadalajara, JAL\n");
    printf("-Mexico Norte, Mexico\n");
    printf("-Poza Rica de Hidalgo, VER\n");
    printf("*****************************************************\n");
break;

//Segundo caso para las proximas solidas
case 2:
    system("cls");
    printf("Nuestras proximas salidas:\n");
    printf("**********************************************************************\n");
    printf("1.-Destino: Acapulco, GRO\n");
    printf("Salida: 06:00 AM  ||  Asientos disponibles: 36  ||  Precio: $699\n");
    printf("Salida: 07:00 AM  ||  Asientos disponibles: 36  ||  Precio: $699\n");
    printf("Salida: 08:00 AM  ||  Asientos disponibles: 36  ||  Precio: $699\n");
    printf("**********************************************************************\n");

    printf("*********************************************************************************\n");
    printf("2.-Destino: Guadalajara, JAL\n");
    printf("Salida: 12:00 PM  ||  Asientos disponibles:36  ||  Precio:$700\n");
    printf("Salida: 13:00 PM  ||  Asientos disponibles:36  ||  Precio:$700\n");
    printf("Salida: 14:00 PM  ||  Asientos disponibles:36  ||  Precio:$700\n");
    printf("*********************************************************************************\n");

    printf("******************************************************************************\n");
    printf("3.-Destino: Mexico Norte, Mexico\n");
    printf("Salida: 06:00 AM  ||  Asientos disponibles:36  ||  Precio:$450\n");
    printf("Salida: 07:00 AM  ||  Asientos disponibles:36  ||  Precio:$450\n");
    printf("Salida: 08:00 AM  ||  Asientos disponibles:36  ||  Precio:$450\n");

    printf("******************************************************************************\n");
    printf("4.-Destino: Poza Rica de Hidalgo, VER\n");
    printf("Salida: 12:00 PM  ||  Asientos disponibles:36  ||  Precio:$450\n");
    printf("Salida: 13:00 PM  ||  Asientos disponibles:36  ||  Precio:$450\n");
    printf("Salida: 14:00 PM  ||  Asientos disponibles:36  ||  Precio:$450\n");
    printf("Salida: 15:00 PM  ||  Asientos disponibles:36  ||  Precio:$450\n");
    printf("******************************************************************************\n");
break;

//Tercer caso para comprar los boletos
case 3:
//Nombre
    system ("cls");
    printf("Nombre:\n");
    printf("Ingrese su nombre por favor:");
    scanf("%s", &nombre);
//Apellido P
    system ("cls");
    printf("Apelliod Paterno:\n");
    printf("Ingrese su apellido paterno:");
    scanf("%s", &apeP);
//Apellido M
    system ("cls");
    printf("Apellido Materno:\n");
    printf("Ingrese su apellido materno:");
    scanf("%s", &apeM);
//Edad
    system ("cls");
    printf("Edad:\n");
    printf("Ingrese su edad:");
    scanf("%i", &edad);
//Tipo de boleto
    system ("cls");
    printf("Tipo de boleto:\n");
    printf("Escribe el tipo de boleto, adulto o ni%co:", 164);
    scanf("%s", &tipoBole);
//Origen
    system ("cls");
    printf("Origen: \n");
    printf("1.-Acapulco, GRO\n");
    printf("2.-Guadalajara, JAL\n");
    printf("3.-Mexico Norte, Mexico\n");
    printf("4.-Poza Rica de Hidalgo, VER\n");
    printf("Escoja su origen por favor:");
    scanf("%i", &origen);

//Declaramos nuestro ciclo en dado caso que el usuario ingrese un origen invalido
    do{
        if(origen >4){
        printf("Lo sentimos, no hay ningun resultado que coincida con su busqueda, seleccione otro:");
        scanf("%i", &origen);
         }
    }while(origen>4); //Agregamos nuestra condicion

//Destino
    system ("cls");
    printf("Destino: \n");
    printf("1.-Acapulco, GRO\n");
    printf("2.-Guadalajara, JAL\n");
    printf("3.-Mexico Norte, Mexico\n");
    printf("4.-Poza Rica de Hidalgo, VER\n");
    printf("Escoja su destino por favor:");
    scanf("%i", &destino);

//Declaramos nuestro ciclo en dado caso que el usuario ingrese un destino incorrecto
    do{
        if(destino >4){
        printf("Lo sentimos, no hay ningun resultado que coincida con su busqueda, seleccione otro:");
        scanf("%i", &destino);

//Esta sentencia se activara en dado caSo que el usuario ingrese como destino su origen
}else if(origen==destino){
                   printf("No puede escojer de destino su lugar de origen, vuelva a intentarlo:");
                    scanf("%i", &destino);
                    }
   }while(destino >4 || origen==destino ); //Agregamos nuestra condicion


//Declaramos nuestras sentencias para asignar el precio del boleto y el total a pagar para el destino 1
   if(destino == 1 ){
        printf("El precio del boleto es de:%i\n", acapulco);
        printf("%cCuantos boletos vas a comprar?:", 168);
        scanf("%i", &cantBol);
        tPagar= cantBol*699;

 do{
            if(cantBol>=5){
            printf("El maximo de boletos para comprar es de 4, vuelvalos a seleccionar:");
            scanf("%i", &cantBol);
             }
 }while(cantBol>=5);

        printf("Bien, ahora elija su salida\n");
        printf("Salida: 6:00 AM\n");
        printf("Salida: 7:00 AM\n");
        printf("Salida: 8:00 AM\n");
        printf("Escriba el primer numero de la salida que desee: ");
        scanf("%i", &salida);

 do{
        if (salida <=5 || salida >=9){
        printf("No hay ninguna salida en ese horario, escribalo de nuevo:");
        scanf("%i", &salida);
        }
 }while(salida <=5 || salida >=9);

    printf("De acuerdo con las salidas mostradas, escriba AM o PM, depende del horario que escogio:");
    scanf("%s", &horario);

//Declaramos nuestras sentencias para asignar el precio del boleto y el total a pagar para el destino 2

 }else if(destino == 2){
        printf("El precio del boleto es de:%i\n", guadalajara);
        printf("%cCuantos boletos vas a comprar?:", 168);
        scanf("%i", &cantBol);
        tPagar= cantBol*700;

 do{
           if(cantBol>=5){
            printf("El maximo de boletos para comprar es de 4, vuelvalos a seleccionar:");
            scanf("%i", &cantBol);
        }
 }while(cantBol>=5);

        printf("Bien, ahora elija su salida\n");
        printf("Salida: 12:00 PM\n");
        printf("Salida: 13:00 PM\n");
        printf("Salida: 14:00 PM\n");
        printf("Escriba el primer numero de la salida que desee: ");
        scanf("%i", &salida);

 do{
        if (salida <=11 || salida >=15){
        printf("No hay ninguna salida en ese horario, escribalo de nuevo:");
        scanf("%i", &salida);
        }
 }while(salida <=11 || salida >=15);

        printf("De acuerdo con las salidas mostradas, escriba AM o PM, depende del horario que escogio:");
        scanf("%s", &horario);

//Declaramos nuestras sentencias para asignar el precio del boleto y el total a pagar para el destino 3

}else if(destino == 3){
        printf("El precio del boleto es de:%i\n", mexico);
        printf("%cCuantos boletos vas a comprar?:",168);
        scanf("%i", &cantBol);
        tPagar= cantBol*450;

 do{
            if(cantBol>=5){
            printf("El maximo de boletos para comprar es de 4, vuelvalos a seleccionar:");
            scanf("%i", &cantBol);
            }
 }while(cantBol>=5);

          printf("Bien, ahora elija su salida\n");
          printf("Salida: 6:00 AM\n");
          printf("Salida: 7:00 AM\n");
          printf("Salida: 8:00 AM\n");
          printf("Escriba el primer numero de la salida que desee: ");
          scanf("%i", &salida);

 do{
          if (salida <=5 || salida >=9){
          printf("No hay ninguna salida en ese horario, escribalo de nuevo:");
          scanf("%i", &salida);
          }
 }while(salida <=5 || salida >=9);

        printf("De acuerdo con las salidas mostradas, escriba AM o PM, depende del horario que escogio:");
        scanf("%s", &horario);

//Declaramos nuestras sentencias para asignar el precio del boleto y el total a pagar para el destino 3

}else if(destino == 4){
        printf("El precio del boleto es de:%i\n", pozaR);
        printf("%cCuantos boletos vas a comprar?:", 168);
        scanf("%i", &cantBol);
        tPagar= cantBol*450;

 do{
            if(cantBol>=5){
            printf("El maximo de boletos para comprar es de 4, vuelvalos a seleccionar:");
            scanf("%i", &cantBol);
            }
 }while(cantBol>=5);

        printf("Bien, ahora elija su salida\n");
        printf("Salida: 12:00 PM\n");
        printf("Salida: 13:00 PM\n");
        printf("Salida: 14:00 PM\n");
        printf("Salida: 15:00 PM\n");
        printf("Escriba el primer numero de la salida que desee: ");
        scanf("%i", &salida);

 do{
         if (salida <=11 || salida >=16){
         printf("No hay ninguna salida en ese horario, escribalo de nuevo:");
         scanf("%i", &salida);
        }
 }while(salida <=11|| salida >=16);

            printf("De acuerdo con las salidas mostradas, escriba AM o PM, depende del horario que escogio:");
            scanf("%s", &horario);
}
//Asientos
    system ("cls");
    printf("Asientos:\n");
    printf("01 05 09 13 17 21 25 29 33\n");
    printf("02 06 10 14 18 22 26 30 34\n");
    printf("                          \n");
    printf("03 07 11 15 19 23 27 31 35\n");
    printf("04 08 12 16 20 24 28 32 36\n");
    printf("Escoja sus asientos, separelos con una coma (12,13,14):");
    scanf("%s", &asientos);

//Operacion para aplicar el descuento al precio de los boletos en general :)
        descuento=tPagar*0.20;
        precioFin=tPagar-descuento;
//Le comunicamos al usuario que su boleto esta listo

    system ("cls");
    printf("%cFelicidades, su boleto esta listo!\n", 173);
    printf("Par ver su boleto oprima 1:");
    scanf("%i", &opcBoleto);

do{
    if(opcBoleto>1){
        system("cls");
        printf("Es necesario que veas tu boleto, escribe el numero uno por favor:");
        scanf("%i", &opcBoleto);
    }
}while(opcBoleto>1);


//Llamamos a la funcion y le pasamos la variables declaradas para la generacion del boleto
   boleto(opcBoleto,nombre,apeP,apeM,edad,origen,destino, cantBol,salida,asientos,precioFin, horario, tipoBole);

   return 0;
}
}while(seleccion !=4);//Agregamos nuestra condicion
}

//Creamos nuestra funcion Boleto
int boleto(opcBoleto,nombre,apeP,apeM,edad,origen,destino, cantBol,salida,asientos,precioFin, horario, tipoBole){

//Declaramos la sentencia switch pata la generacion del boleto
switch(opcBoleto){

case 1:
    printf("*************************************************\n");
    printf("Marca comercial: Bus MX\n");
    printf("Razon social: AUTOBUSES BUS MX S.A DE C.V.\n");
    printf("Nombre: %s\n", nombre);
    printf("Apellido paterno: %s\n", apeP);
    printf("Apllido Materno: %s\n", apeM);
    printf("Edad: %i\n", edad);
    printf("Tipo de boleto: %s\n", tipoBole);
    printf("Origen: %i\n", origen);
    printf("Destino: %i\n", destino);
    printf("Servicio: PRIM SELECT\n");
    printf("Salida: %i\n", salida);
    printf("Horario: %s\n", horario);
    printf("Sala: 1-A\n");
    printf("Boletos adquiridos: %i\n", cantBol);
    printf("Tus asientos: %s\n", asientos);
    printf("Total a pagar: %i\n", precioFin);
    printf("Imprima este boleto y acuda a pagar a cualquier super de su referencia\n");
    printf("*************************************************\n");
    printf("Te dejamos esta guia para que veas tu origen y estino de acuerdo a los numeros\n");
    printf("1.-Acapulco, GRO\n");
    printf("2.-Guadalajara, JAL\n");
    printf("3.-Mexico Norte, Mexico\n");
    printf("4.-Poza Rica de Hidalgo, VER\n");
break;
}
}
